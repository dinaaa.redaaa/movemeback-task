# from django.shortcuts import render
# from django.utils import timezone
# from .models import Post
from django.shortcuts import render, get_object_or_404

# from wagtail.core.models import Page
# from wagtail.core.fields import RichTextField
# from wagtail.admin.edit_handlers import FieldPanel

# def post_list(request):
#     # posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
#     # return render(request, 'blog/post_list.html', {'posts': posts})
#     # posts = Post.objects.order_by('id')
#     # print(Post.objects)
#     return render(request, 'blog/post_list.html', {})
#
# def post_detail(request, pk):
#     post = get_object_or_404(Post, pk=pk)
#     return render(request, 'blog/post_detail.html', {'post': post})
#
def home_page(request):
    return render(request, 'home/home_page.html', {})
